import { getAbs } from "./get_abs";

export function mergeAsTwo(a, b) {
  return [getAbs(a[0] * b[0] - a[1] * b[1]), a[0] * b[1] + a[1] * b[0], 0n, 0n];
}