import { getBitWidth } from "./get_bit_width";
import { getMin } from "./get_min";

export function getIntSqrt(n) {
  if (n <= 1n) {
    return n;
  }

  let x0 = n;
  let x1 = getMin(1n << (getBitWidth(n) / 2n + 1n), n / 2n);

  while (x1 < x0) {
    x0 = x1;
    x1 = (x0 + n / x0) / 2n;
  }

  return x0;
}