import { getAbs } from "./get_abs";
import { getGCD } from "./get_gcd";
import { isPrime } from "./is_prime";
import { RNG } from "./rng";

export function factorizeRecursively(n, result) {
  if (n <= 1n) {
    return;
  } else if (isPrime(n)) {
    result.push(n);
    return;
  } else if (n % 2n === 0n) {
    result.push(2n);
    factorizeRecursively(n / 2n, result);
    return;
  }

  const rng = new RNG();
  let xs,
    xt,
    c,
    factor = n;
  const f = (x) => (((x * x) % n) + c) % n;

  while (factor === 1n || factor === n) {
    if (factor === n) {
      xs = xt = rng.nextRange(n - 2n) + 2n;
      c = rng.nextRange(20n) + 1n;
    }
    xs = f(xs);
    xt = f(f(xt));
    factor = getGCD(getAbs(xs - xt), n);
  }

  factorizeRecursively(factor, result);
  factorizeRecursively(n / factor, result);
}

export function factorize(n) {
  const result = [];
  factorizeRecursively(n, result);
  return result.sort((a, b) => (a < b ? -1 : a > b ? 1 : 0));
}