import { getBitWidth } from "./get_bit_width";

export class RNG {
  constructor() {
    this.seed = 1n;
  }

  next31Bit() {
    this.seed = (1103515245n * this.seed + 12345n) % 0x7fffffffn;
    return this.seed;
  }

  nextRange(bound) {
    const width = getBitWidth(bound);
    let result = 0n;
    for (let pos = 0n; pos < width; pos += 31n) {
      result = (result << 31n) | this.next31Bit();
    }
    return result % bound;
  }
}