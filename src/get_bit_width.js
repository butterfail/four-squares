export function getBitWidth(n) {
  let e = 1n;
  while (1n << e <= n) {
    ++e;
  }
  return e - 1n;
}