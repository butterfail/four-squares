import { getModPow } from "./get_mod_pow";

export function doMillerRabinTest(n, a) {
  let d = n - 1n;
  while (d % 2n === 0n) {
    d /= 2n;
  }

  let x = getModPow(a, d, n);
  if (x === 1n || x === n - 1n) {
    return true;
  }

  while (d !== n - 1n) {
    x = (x * x) % n;
    d *= 2n;
    if (x === n - 1n) {
      return true;
    }
  }

  return false;
}

export function doMillerRabinTestWithArray(n, as) {
  return as.every((a) => doMillerRabinTest(n, a));
}