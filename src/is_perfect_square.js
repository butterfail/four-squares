import { getIntSqrt } from "./get_int_sqrt";

export function isPerfectSquare(n) {
  const sqrt = getIntSqrt(n);
  return sqrt * sqrt === n;
}