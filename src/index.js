import { RABIN_EXCEPTIONS } from "./constants";
import { getAbs } from "./get_abs";
import { getIntSqrt } from "./get_int_sqrt";
import { getModPow } from "./get_mod_pow";
import { getRemainders } from "./get_remainders";
import { isPerfectSquare } from "./is_perfect_square";
import { isPrime } from "./is_prime";
import { RNG } from "./rng";

export function findSumOfTwo(p) {
  if (p === 1n) {
    return [1n, 0n, 0n, 0n];
  }
  if (p === 2n) {
    return [1n, 1n, 0n, 0n];
  }

  let rems = [];
  const rng = new RNG();
  do {
    let q = rng.nextRange(p);
    while (getModPow(q, (p - 1n) / 2n, p) !== p - 1n) {
      q = rng.nextRange(p);
    }
    const x = getModPow(q, (p - 1n) / 4n, p);
    rems = getRemainders(p, x, p);
  } while (rems.length <= 2);
  return [rems[0], rems[1], 0n, 0n];
}

export function findSumOfThree(n) {
  if (RABIN_EXCEPTIONS[n]) {
    return RABIN_EXCEPTIONS[n];
  } else if (n % 4n === 0n) {
    const sub = findSumOfThree(n / 4n);
    return [sub[0] * 2n, sub[1] * 2n, sub[2] * 2n, sub[3] * 2n];
  } else if (n % 8n === 7n) {
    return [0n, 0n, 0n, 0n]; // Exception - Impossible
  } else if (n % 8n === 3n) {
    let x, p;
    const rng = new RNG();
    do {
      x = rng.nextRange(getIntSqrt(n) + 1n);
      p = (n - x * x) / 2n;
    } while ((n - x * x) % 2n !== 0n || !(isPrime(p) || p === 1n));
    const two = findSumOfTwo(p);
    return [x, two[0] + two[1], getAbs(two[0] - two[1]), 0n];
  } else if (isPerfectSquare(n)) {
    return [getIntSqrt(n), 0n, 0n, 0n];
  } else {
    let x, p;
    const rng = new RNG();
    do {
      x = rng.nextRange(getIntSqrt(n) + 1n);
      p = n - x * x;
    } while (!isPrime(p));
    const two = findSumOfTwo(p);
    return [x, two[0], two[1], 0n];
  }
}

export function fourSquares(n) {
  if (n === 1n) {
    return [1n, 0n, 0n, 0n];
  } else if (n === 2n) {
    return [1n, 1n, 0n, 0n];
  } else if (n === 3n) {
    return [1n, 1n, 1n, 0n];
  }

  if (isPerfectSquare(n)) {
    return [getIntSqrt(n), 0n, 0n, 0n];
  }

  if (n % 8n === 7n) {
    const sub = findSumOfThree(n - 4n);
    return [2n, sub[0], sub[1], sub[2]];
  }

  return findSumOfThree(n);
}

const blue = '\x1b[34m';
const reset = '\x1b[0m';
const yellow = '\x1b[33m';
const green = '\x1b[32m';
const red = '\x1b[31m';
const msColors = (ms) => {
  return `${ms < 12000 ? green : red}${ms}ms${reset}`;
}

const testCases = [
  0n,
  1n,
  17n,
  33n,
  215n,
  333n,
  2n ** 12n - 3n, 
  1234567890n,
]

const rng = new RNG();
for (let i = 0; i < 20; i++) {
  testCases.push(rng.nextRange(2n ** 128n));
}

for (let i = 0; i < 20; i++) {
  testCases.push(rng.nextRange(2n ** 1024n));
}

const tt1 = performance.now();
for (let n of testCases) {
  let [a, b, c, d] = fourSquares(n);
  const t1 = performance.now();

  if (a * a + b * b + c * c + d * d === n) {
    console.log(`${blue}${n} ${reset}=== ${blue}${a}${yellow}^2 ${reset}+ ${blue}${b}${yellow}^2 ${reset}+ ${blue}${c}${yellow}^2 ${reset}+ ${blue}${d}${yellow}^2${reset}`);
    console.log(`Solve in: ${msColors(performance.now() - t1)}${reset}\n`);
  } else {
    console.error(`${red}${n} ${reset}!= ${red}${a}${reset}^2 + ${red}${b}${reset}^2 + ${red}${c}${reset}^2 + ${red}${d}${reset}^2`);
    break;
  }
}
console.log(`Total time: ${msColors(performance.now() - tt1)}`);