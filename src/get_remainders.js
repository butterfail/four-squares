import { getIntSqrt } from "./get_int_sqrt";

export function getRemainders(a, b, bound) {
  const boundSqrt = getIntSqrt(bound);
  const result = [];
  while (b !== 0n) {
    const r = a % b;
    if (r <= boundSqrt) {
      result.push(r);
    }
    a = b;
    b = r;
  }
  return result;
}