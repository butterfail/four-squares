export function getModPow(val, exp, mod) {
  let result = 1n,
    subPower = val % mod;
  while (exp > 0n) {
    if ((exp & 1n) === 1n) {
      result = (result * subPower) % mod;
    }
    exp >>= 1n;
    subPower = (subPower * subPower) % mod;
  }
  return result;
}