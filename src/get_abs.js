export function getAbs(a) {
  return a > 0 ? a : -a;
}