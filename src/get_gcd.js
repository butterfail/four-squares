export function getGCD(a, b) {
  return b === 0n ? a : getGCD(b, a % b);
}