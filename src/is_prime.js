import { MERSENNE_PRIME_EXPONENTS, ROBUST_TEST_SET } from "./constants";
import { getModPow } from "./get_mod_pow";
import { doMillerRabinTest, doMillerRabinTestWithArray } from "./miller_rabin";
import { RNG } from "./rng";

export function isPrime(n) {
  if ([2n, 3n, 5n].includes(n)) {
    return true;
  }
  if (n <= 1n || n % 2n === 0n || n % 3n === 0n) {
    return false;
  }
  if (n < 49n) {
    return true;
  }
  if (
    n % 7n === 0n ||
    n % 11n === 0n ||
    n % 13n === 0n ||
    n % 17n === 0n ||
    n % 19n === 0n ||
    n % 23n === 0n ||
    n % 29n === 0n ||
    n % 31n === 0n ||
    n % 37n === 0n ||
    n % 41n === 0n ||
    n % 43n === 0n ||
    n % 47n === 0n
  ) {
    return false;
  }
  if (n < 2809n) {
    return true;
  }
  if (n < 65077n) {
    // # There are only five Euler pseudoprimes with a least prime factor greater than 47
    return (
      [1n, n - 1n].includes(getModPow(2n, n >> 1n, n)) &&
      ![8321n, 31621n, 42799n, 49141n, 49981n].includes(n)
    );
  }
  if (MERSENNE_PRIME_EXPONENTS.includes(n)) {
    return true;
  }

  /**
   * deterministic Miller-Rabin testing for numbers < 2^64.
   * @see https://miller-rabin.appspot.com/
   */
  if (n < 341531n) {
    return doMillerRabinTestWithArray(n, [9345883071009581737n]);
  }
  if (n < 885594169n) {
    return doMillerRabinTestWithArray(n, [725270293939359937n, 3569819667048198375n]);
  }
  if (n < 350269456337n) {
    return doMillerRabinTestWithArray(n, [
      4230279247111683200n,
      14694767155120705706n,
      16641139526367750375n,
    ]);
  }
  if (n < 55245642489451n) {
    return doMillerRabinTestWithArray(n, [
      2n,
      141889084524735n,
      1199124725622454117n,
      11096072698276303650n,
    ]);
  }
  if (n < 7999252175582851n) {
    return doMillerRabinTestWithArray(n, [
      2n,
      4130806001517n,
      149795463772692060n,
      186635894390467037n,
      3967304179347715805n,
    ]);
  }
  if (n < 585226005592931977n) {
    return doMillerRabinTestWithArray(n, [
      2n,
      123635709730000n,
      9233062284813009n,
      43835965440333360n,
      761179012939631437n,
      1263739024124850375n,
    ]);
  }
  if (n < 18446744073709551616n) {
    return doMillerRabinTestWithArray(n, [
      2n,
      325n,
      9375n,
      28178n,
      450775n,
      9780504n,
      1795265022n,
    ]);
  }
  if (n < 318665857834031151167461n) {
    return doMillerRabinTestWithArray(n, [
      2n,
      3n,
      5n,
      7n,
      11n,
      13n,
      17n,
      19n,
      23n,
      29n,
      31n,
      37n,
    ]);
  }
  if (n < 3317044064679887385961981n) {
    return doMillerRabinTestWithArray(n, [
      2n,
      3n,
      5n,
      7n,
      11n,
      13n,
      17n,
      19n,
      23n,
      29n,
      31n,
      37n,
      41n,
    ]);
  }

  let robustTests = ROBUST_TEST_SET.find(([bound]) => n < bound)?.[1];
  if (robustTests === undefined) {
    const rng = new RNG();
    robustTests = Array.from({ length: 15 }, () => rng.next31Bit());
  }
  return robustTests.every((a) => {
    return doMillerRabinTest(n, a);
  });
}
